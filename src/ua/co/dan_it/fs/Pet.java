package ua.co.dan_it.fs;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species; //вид животного
    private String nickname; // кличка
    private int age; // возраст
    private int trickLevel; // уровень хитрости (целое число от 0 до 100)
    private String[] habits = new String[10]; //привычки

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname.substring(0, 1).toUpperCase() + nickname.substring(1).toLowerCase();
        ;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {

    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    void eat() { //выводит на экран сообщение `Я кушаю!`
        System.out.println("Я кушаю!");
    }

    void respond(String nickname) { //метод отозваться
        nickname = this.getNickname();
        System.out.println("Привет, хозяин. Я - " + nickname + " соскучился!");
    }

    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();
        for (String habbit : this.habits) {
            stringBuilder.append(habbit + ", ");
        }
        String petHabits = stringBuilder.toString();

        result.append(this.species);
        result.append(" {nickname = ");
        result.append(this.nickname);
        result.append(", age = ");
        result.append(this.age);
        result.append(", trickLevel = ");
        result.append(this.trickLevel);
        result.append(", habits = [");
        result.append(petHabits);
        result.append(" ]}");
        return result.toString();
    }
}
