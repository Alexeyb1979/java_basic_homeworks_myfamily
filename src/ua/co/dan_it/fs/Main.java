package ua.co.dan_it.fs;

public class Main {
    public static void main(String[] args) {

        Pet dog = new Pet("dog", "Bobik");

        Pet cat = new Pet("cat","Tomas",5,58, new String[]{"eat", "sleep"});

        Pet rabbit = new Pet();
        rabbit.setNickname("banny");

        System.out.println(cat.toString());

        Human father = new Human("Peter", "Yacobs", 1958);
        Human bobbiesWife = new Human ("Elisa", "Jonson", 1986);
        Human bobbiesSun = new Human("Bill", "Yacobs",2000, new String[7][2]);
        Human bobbiesNephew = new Human();

        Family family = new Family(father, bobbiesWife);
        family.addChild(bobbiesSun);
        family.setPet(cat);
        System.out.println(family.toString());

        family.removeChild(bobbiesSun);
        family.removeChild(bobbiesSun);
        System.out.println(family.toString());
    }
}
