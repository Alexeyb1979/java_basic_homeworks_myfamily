package ua.co.dan_it.fs;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother = new Human();
    private Human father = new Human();
    private Human[] children = new Human[0];
    private Pet pet = new Pet();


    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        father.setFamily(this);
        mother.setFamily(this);
    }

    void greetPet() {
        System.out.println("Привет, " + this.pet.getNickname() + "!");
    }

    void describePet (){
        String trick;
        if (this.pet.getTrickLevel() > 50){
            trick = "очень хитрый";
        } else {
            trick = "почти не хитрый";
        }

        System.out.println ("У меня есть" + this.pet.getSpecies() + ", ему " + this.pet.getAge() + " лет, он" + trick + ".");

    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    //добавить ребенка `addChild` (принимает тип `Human` и добавляет его в массив детей; добавляет ребенку
    // ссылку на текущую семью)

    public void addChild(Human child) {
        Human[] tempChildrenArray = new Human[children.length + 1];
        System.arraycopy(children, 0, tempChildrenArray, 0, children.length);
        tempChildrenArray[tempChildrenArray.length - 1] = child;
        child.setFamily(this);
        children = tempChildrenArray;
    }

    public void removeChild(Human removedChild) {
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (removedChild.equals(children[i])){
                index = i;
            }
        }
        if (index == -1){
            return;
        } else {
            Human[] tempChildrenArray = new Human[children.length - 1];
            int j = 0;
            for (Human child : children) {

                if (removedChild.equals(child)) {
                    child.setFamily(null);
                    continue;
                } else {

                    tempChildrenArray[j] = child;
                    j++;
                }
                children = tempChildrenArray;
            }
        }
    }

    public void countFamily() {
        System.out.println("This family consist of " + 2 + this.children.length + " members.");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) &&
                father.equals(family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();

        for (Human child : this.getChildren()) {
            stringBuilder.append(child.toString() + ", ");
        }
        String childrenList = stringBuilder.toString();

        result.append("Family {Mother: ");
        result.append(this.mother.toString());
        result.append(", Father: ");
        result.append(this.father.toString());
        result.append(", Children: ");
        result.append(childrenList);
        result.append(", pet= ");
        result.append(this.pet.toString());
        result.append("}");
        return result.toString();
    }
}

