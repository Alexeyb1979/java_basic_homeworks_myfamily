package ua.co.dan_it.fs;

import java.util.Arrays;
import java.util.Objects;

public class Human {


    private String name;
    private String surname;
    private int year;
    private int iq;

    private String [][] schedule = new String[7][2];
    private Family family;
    
    Human(String name, String surname, int year) {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();;
        this.surname = surname;
        this.year = year;
    }

    Human(String name, String surname, int year, String [][] schedule){
        this(name, surname, year);
        this.schedule = schedule;
     }

     Human(){

     }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }
   public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }


    @Override
    public String toString(){

       return "Human {name = " + this.name + ", surname = " + this.surname + ", year = " + this.year +
               ", iq = " + this.iq;
   }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, family);
    }
}
